// Get the form and output elements
const loanForm = document.querySelector('#loan-calculator form');
const output = document.querySelector('#loan-section #loan-calc-output');

// Add event listener for form submit
loanForm.addEventListener('submit', (e) => {
  e.preventDefault();
  calculateLoan();
});

// Function to calculate the loan
function calculateLoan() {
  // Get the form input values
  const mortgageAmount = parseFloat(document.querySelector('#mortgage-amount').value);
  const downPayment = parseFloat(document.querySelector('#down-payment').value);
  const loanTerm = parseFloat(document.querySelector('#loan-term').value);
  const interestRate = parseFloat(document.querySelector('#interest-rate').value);
  const monthlyInsurance = parseFloat(document.querySelector('#monthly-insurance').value);
  const monthlyExtraPayment = parseFloat(document.querySelector('#monthly-extra-payment').value);
  const monthlyHoa = parseFloat(document.querySelector('#monthly-hoa').value);
  const yearlyPropertyTax = parseFloat(document.querySelector('#property-tax').value);

  // Calculate the loan payments
  const principal = (mortgageAmount - downPayment);
  const interestPercentage = ((interestRate / 100) / 12);
  const monthlyPaymentCount = (loanTerm * 12);
  const monthlyPrincipalAndInterest = ((principal * interestPercentage) / (1 - Math.pow(1 + interestPercentage, -monthlyPaymentCount)));
  const monthlyPmi = (monthlyPrincipalAndInterest * 0.0589862523750978);
  const totalExtraPayment = (monthlyExtraPayment * 12 * loanTerm);
  const extraPaymentInterest = (monthlyExtraPayment * interestPercentage);
  const monthlyPayment = (monthlyPrincipalAndInterest + monthlyHoa + (yearlyPropertyTax / 12) + monthlyPmi + monthlyInsurance + monthlyExtraPayment);
  const totalInterest = (monthlyPrincipalAndInterest * monthlyPaymentCount) - principal;
  const totalPayment = ((monthlyPayment - monthlyExtraPayment) - (monthlyExtraPayment - extraPaymentInterest)) * monthlyPaymentCount;

  // Output the results to the page
  output.querySelector('#total-payment').textContent = '$' + totalPayment.toFixed(2);
  output.querySelector('#monthly-payment').textContent = '$' + monthlyPayment.toFixed(2);
  output.querySelector('#mortgage-amount-output').textContent = '$' + mortgageAmount.toFixed(2);
  output.querySelector('#total-interest').textContent = '$' + totalInterest.toFixed(2);
  output.querySelector('#principal-output').textContent = '$' + principal.toFixed(2);
  output.querySelector('#down-payment-output').textContent = '$' + downPayment.toFixed(2);
  output.querySelector('#monthly-principal-interest').textContent = '$' + monthlyPrincipalAndInterest.toFixed(2);
  output.querySelector('#total-extra-payment-output').textContent = '$' + totalExtraPayment.toFixed(2);
}
